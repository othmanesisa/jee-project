/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upem.jee.listener;

/**
 *
 * @author Surhomme
 */


import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

@ManagedBean
@SessionScoped
public class ClassListener {
	
	public void valueChange(ValueChangeEvent e){
		System.out.println(">> ValueChange() : " + e.getNewValue());
		FacesContext context = FacesContext.getCurrentInstance();
		context.getViewRoot().setLocale(new Locale(e.getNewValue().toString()));
	}
}

