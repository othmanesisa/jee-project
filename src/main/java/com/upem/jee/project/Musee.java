/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upem.jee.project;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Surhomme
 */
@Entity
@Table(name = "musee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Musee.findAll", query = "SELECT m FROM Musee m")
    , @NamedQuery(name = "Musee.findByNomreg", query = "SELECT m FROM Musee m WHERE m.nomreg = :nomreg")
    , @NamedQuery(name = "Musee.findByNomdep", query = "SELECT m FROM Musee m WHERE m.nomdep = :nomdep")
    , @NamedQuery(name = "Musee.findByDateappellation", query = "SELECT m FROM Musee m WHERE m.dateappellation = :dateappellation")
    , @NamedQuery(name = "Musee.findByFerme", query = "SELECT m FROM Musee m WHERE m.ferme = :ferme")
    , @NamedQuery(name = "Musee.findByAnnreouv", query = "SELECT m FROM Musee m WHERE m.annreouv = :annreouv")
    , @NamedQuery(name = "Musee.findByAnnexe", query = "SELECT m FROM Musee m WHERE m.annexe = :annexe")
    , @NamedQuery(name = "Musee.findByNomDuMusee", query = "SELECT m FROM Musee m WHERE m.nomDuMusee = :nomDuMusee")
    , @NamedQuery(name = "Musee.findByAdr", query = "SELECT m FROM Musee m WHERE m.adr = :adr")
    , @NamedQuery(name = "Musee.findByCp", query = "SELECT m FROM Musee m WHERE m.cp = :cp")
    , @NamedQuery(name = "Musee.findByVille", query = "SELECT m FROM Musee m WHERE m.ville = :ville")
    , @NamedQuery(name = "Musee.findBySitweb", query = "SELECT m FROM Musee m WHERE m.sitweb = :sitweb")
    , @NamedQuery(name = "Musee.findByFermetureAnnuelle", query = "SELECT m FROM Musee m WHERE m.fermetureAnnuelle = :fermetureAnnuelle")
    , @NamedQuery(name = "Musee.findByPeriodeOuverture", query = "SELECT m FROM Musee m WHERE m.periodeOuverture = :periodeOuverture")
    , @NamedQuery(name = "Musee.findByJoursNocturnes", query = "SELECT m FROM Musee m WHERE m.joursNocturnes = :joursNocturnes")
    , @NamedQuery(name = "Musee.findById", query = "SELECT m FROM Musee m WHERE m.id = :id")
    , @NamedQuery(name = "Musee.findByVill", query = "SELECT m FROM Musee m WHERE m.vill = :vill")})
public class Musee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 26)
    @Column(name = "NOMREG")
    private String nomreg;
    @Size(max = 24)
    @Column(name = "NOMDEP")
    private String nomdep;
    @Column(name = "DATEAPPELLATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateappellation;
    @Size(max = 3)
    @Column(name = "FERME")
    private String ferme;
    @Size(max = 46)
    @Column(name = "ANNREOUV")
    private String annreouv;
    @Size(max = 46)
    @Column(name = "ANNEXE")
    private String annexe;
    @Size(max = 94)
    @Column(name = "NOM_DU_MUSEE")
    private String nomDuMusee;
    @Size(max = 93)
    @Column(name = "ADR")
    private String adr;
    @Column(name = "CP")
    private Integer cp;
    @Size(max = 36)
    @Column(name = "VILLE")
    private String ville;
    @Size(max = 137)
    @Column(name = "SITWEB")
    private String sitweb;
    @Size(max = 248)
    @Column(name = "FERMETURE_ANNUELLE")
    private String fermetureAnnuelle;
    @Size(max = 255)
    @Column(name = "PERIODE_OUVERTURE")
    private String periodeOuverture;
    @Size(max = 75)
    @Column(name = "JOURS_NOCTURNES")
    private String joursNocturnes;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "VILL")
    private String vill;

    public Musee() {
    }

    public Musee(Integer id) {
        this.id = id;
    }

    public String getNomreg() {
        return nomreg;
    }

    public void setNomreg(String nomreg) {
        this.nomreg = nomreg;
    }

    public String getNomdep() {
        return nomdep;
    }

    public void setNomdep(String nomdep) {
        this.nomdep = nomdep;
    }

    public Date getDateappellation() {
        return dateappellation;
    }

    public void setDateappellation(Date dateappellation) {
        this.dateappellation = dateappellation;
    }

    public String getFerme() {
        return ferme;
    }

    public void setFerme(String ferme) {
        this.ferme = ferme;
    }

    public String getAnnreouv() {
        return annreouv;
    }

    public void setAnnreouv(String annreouv) {
        this.annreouv = annreouv;
    }

    public String getAnnexe() {
        return annexe;
    }

    public void setAnnexe(String annexe) {
        this.annexe = annexe;
    }

    public String getNomDuMusee() {
        return nomDuMusee;
    }

    public void setNomDuMusee(String nomDuMusee) {
        this.nomDuMusee = nomDuMusee;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public Integer getCp() {
        return cp;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getSitweb() {
        return sitweb;
    }

    public void setSitweb(String sitweb) {
        this.sitweb = sitweb;
    }

    public String getFermetureAnnuelle() {
        return fermetureAnnuelle;
    }

    public void setFermetureAnnuelle(String fermetureAnnuelle) {
        this.fermetureAnnuelle = fermetureAnnuelle;
    }

    public String getPeriodeOuverture() {
        return periodeOuverture;
    }

    public void setPeriodeOuverture(String periodeOuverture) {
        this.periodeOuverture = periodeOuverture;
    }

    public String getJoursNocturnes() {
        return joursNocturnes;
    }

    public void setJoursNocturnes(String joursNocturnes) {
        this.joursNocturnes = joursNocturnes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVill() {
        return vill;
    }

    public void setVill(String vill) {
        this.vill = vill;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Musee)) {
            return false;
        }
        Musee other = (Musee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.upem.jee.project.Musee[ id=" + id + " ]";
    }
    
}
