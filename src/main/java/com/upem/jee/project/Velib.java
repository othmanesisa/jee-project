/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upem.jee.project;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Surhomme
 */
@Entity
@Table(name = "velib")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Velib.findAll", query = "SELECT v FROM Velib v")
    , @NamedQuery(name = "Velib.findByIdentifiantDsp", query = "SELECT v FROM Velib v WHERE v.identifiantDsp = :identifiantDsp")
    , @NamedQuery(name = "Velib.findByCodePostal", query = "SELECT v FROM Velib v WHERE v.codePostal = :codePostal")
    , @NamedQuery(name = "Velib.findByCodeDepartement", query = "SELECT v FROM Velib v WHERE v.codeDepartement = :codeDepartement")
    , @NamedQuery(name = "Velib.findByVille", query = "SELECT v FROM Velib v WHERE v.ville = :ville")
    , @NamedQuery(name = "Velib.findByTypeDeStation", query = "SELECT v FROM Velib v WHERE v.typeDeStation = :typeDeStation")
    , @NamedQuery(name = "Velib.findByEmplacement", query = "SELECT v FROM Velib v WHERE v.emplacement = :emplacement")
    , @NamedQuery(name = "Velib.findByLocalisation", query = "SELECT v FROM Velib v WHERE v.localisation = :localisation")
    , @NamedQuery(name = "Velib.findByNomDeLaStation", query = "SELECT v FROM Velib v WHERE v.nomDeLaStation = :nomDeLaStation")
    , @NamedQuery(name = "Velib.findByPlacesAutolib", query = "SELECT v FROM Velib v WHERE v.placesAutolib = :placesAutolib")
    , @NamedQuery(name = "Velib.findByPlacesRechargeTiers", query = "SELECT v FROM Velib v WHERE v.placesRechargeTiers = :placesRechargeTiers")
    , @NamedQuery(name = "Velib.findByNombreTotalDePlaces", query = "SELECT v FROM Velib v WHERE v.nombreTotalDePlaces = :nombreTotalDePlaces")
    , @NamedQuery(name = "Velib.findById", query = "SELECT v FROM Velib v WHERE v.id = :id")})
public class Velib implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "identifiant_dsp")
    private String identifiantDsp;
    @Column(name = "code_postal")
    private Integer codePostal;
    @Column(name = "code_departement")
    private Integer codeDepartement;
    @Size(max = 21)
    @Column(name = "ville")
    private String ville;
    @Size(max = 7)
    @Column(name = "type_de_station")
    private String typeDeStation;
    @Size(max = 7)
    @Column(name = "emplacement")
    private String emplacement;
    @Size(max = 21)
    @Column(name = "localisation")
    private String localisation;
    @Size(max = 46)
    @Column(name = "nom_de_la_station")
    private String nomDeLaStation;
    @Column(name = "places_autolib")
    private Integer placesAutolib;
    @Column(name = "places_recharge_tiers")
    private Integer placesRechargeTiers;
    @Column(name = "nombre_total_de_places")
    private Integer nombreTotalDePlaces;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;

    public Velib() {
    }

    public Velib(Integer id) {
        this.id = id;
    }

    public Velib(Integer id, String identifiantDsp) {
        this.id = id;
        this.identifiantDsp = identifiantDsp;
    }

    public String getIdentifiantDsp() {
        return identifiantDsp;
    }

    public void setIdentifiantDsp(String identifiantDsp) {
        this.identifiantDsp = identifiantDsp;
    }

    public Integer getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(Integer codePostal) {
        this.codePostal = codePostal;
    }

    public Integer getCodeDepartement() {
        return codeDepartement;
    }

    public void setCodeDepartement(Integer codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTypeDeStation() {
        return typeDeStation;
    }

    public void setTypeDeStation(String typeDeStation) {
        this.typeDeStation = typeDeStation;
    }

    public String getEmplacement() {
        return emplacement;
    }

    public void setEmplacement(String emplacement) {
        this.emplacement = emplacement;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public String getNomDeLaStation() {
        return nomDeLaStation;
    }

    public void setNomDeLaStation(String nomDeLaStation) {
        this.nomDeLaStation = nomDeLaStation;
    }

    public Integer getPlacesAutolib() {
        return placesAutolib;
    }

    public void setPlacesAutolib(Integer placesAutolib) {
        this.placesAutolib = placesAutolib;
    }

    public Integer getPlacesRechargeTiers() {
        return placesRechargeTiers;
    }

    public void setPlacesRechargeTiers(Integer placesRechargeTiers) {
        this.placesRechargeTiers = placesRechargeTiers;
    }

    public Integer getNombreTotalDePlaces() {
        return nombreTotalDePlaces;
    }

    public void setNombreTotalDePlaces(Integer nombreTotalDePlaces) {
        this.nombreTotalDePlaces = nombreTotalDePlaces;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Velib)) {
            return false;
        }
        Velib other = (Velib) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.upem.jee.project.Velib[ id=" + id + " ]";
    }
    
}
